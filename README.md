# IP Geolocation

This is a demo project of IP Geolocation.

## Install

```sh
$ npm install
```
Make sure that [git](http://git-scm.com/) is installed as some packages require it to be fetched and installed.

### Installing packages and dependencies

```sh
# install dependencies listed in bower.json
$ bower install
```


## Usage

### Run the server

```sh
$ npm start
```

Since the default port is set to `3000`, make sure that this port is available to use.

### Open on the browser the following URLs for:

- UI: [http://localhost:3000/api?ip=1.1.1.1](http://localhost:3000/api?ip=1.1.1.1)
- JSON: [http://localhost:3000/api?ip=1.1.1.1&format=json](http://localhost:3000/api?ip=1.1.1.1&format=json)