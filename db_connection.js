var config = require('./config/db_config');
var mysql = require('mysql');

var connection = mysql.createConnection({
    host: config.host,
    user: config.username,
    password: config.password,
    database: config.database
});

module.exports = connection;