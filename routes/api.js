var express = require('express');
var router = express.Router();
var request = require('request');

/* GET IP information. */
router.get('/', function(req, res, next) {
    var format = req.query.format;
    if (typeof req.query.ip != "undefined") {
        request('http://ip-api.com/json/' + req.query.ip, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                if (typeof format != "undefined" && format == "json") {
                    res.send(body);
                } else {
                    try {
                        body = JSON.parse(body);
                        res.render('api', { data: body });
                    } catch (err) {
                        console.log(err);
                    }
                }
            }
        });
    } else {
        res.send('API query is not valid!');
    }
});

module.exports = router;